import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../environments/environment';
import * as $ from 'jquery';




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  public appPages = [
    
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Teacher',
      url: '/teacher',
      icon: 'person'
    },
    {
      title: 'Noticeboard',
      url: '/noticeboard',
      icon: 'clipboard'
    },
    {
      title: 'Home Work',
      url: '/homework',
      icon: 'bookmarks'
    },
    {
      title: 'Exam Marks',
      url: '/exammarks',
      icon: 'paper'
    },
    {
      title: 'Timetable',
      url: '/timetable',
      icon: 'time'
    },
    {
      title: 'Attendance',
      url: '/attendance',
      icon: 'list-box'
    },
    {
      title: 'Flash News',
      url: '/flashnews',
      icon: 'flash'
    },
    {
      title: 'Video',
      url: '/videgallery',
      icon: 'videocam'
    },
    {
      title: 'Gallery',
      url: '/gallery',
      icon: 'images'
    },
    {
      title: 'Feed Back',
      url: '/feedback',
      icon: 'paper'
    },
    {
      title: 'Manage Profile',
      url: '/profile',
      icon: 'person-add'
    },
    
  ];
  
  data: any;
 
  constructor(
    private http: HttpClient,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
 

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });



    var token = localStorage.getItem("token");
    /* console.log(token); */
    var apiUrl = environment.apiUrl+'parentapi/getchildren';
    this.http.get(apiUrl,{
    headers: {'Content-Type':'application/json','Authorization':token}
  })

  .map(res => res)
  .subscribe(data => {
    this.data = data;
    console.log(data);
    if (data["status"] == "success") {
 
      var html = " ";
      this.data.data.forEach(function(val) {
      var keys = Object.keys(val);
         
      html +='<ion-avatar><img src="'+environment.apiUrl+'uploads/student_image/'+val.student_img+'"></ion-avatar><div class="st-name"><ion-label>'+val.name+' </ion-label></div>';
     
         
      $(".studentimg").html(html);
     
      });
 
     
    }
 
  });


    
  }
}
