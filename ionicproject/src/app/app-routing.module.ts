import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'otp', loadChildren: './pages/otp/otp.module#OtpPageModule' },
  { path: 'teacher', loadChildren: './pages/teacher/teacher.module#TeacherPageModule' },
  { path: 'exammarks', loadChildren: './pages/exammarks/exammarks.module#ExammarksPageModule' },
  { path: 'feedback', loadChildren: './pages/feedback/feedback.module#FeedbackPageModule' },
  { path: 'flashnews', loadChildren: './pages/flashnews/flashnews.module#FlashnewsPageModule' },
  { path: 'gallery', loadChildren: './pages/gallery/gallery.module#GalleryPageModule' },
  { path: 'homework', loadChildren: './pages/homework/homework.module#HomeworkPageModule' },
  { path: 'noticeboard', loadChildren: './pages/noticeboard/noticeboard.module#NoticeboardPageModule' },
  { path: 'timetable', loadChildren: './pages/timetable/timetable.module#TimetablePageModule' },
  { path: 'videgallery', loadChildren: './pages/videgallery/videgallery.module#VidegalleryPageModule' },
  { path: 'attendance', loadChildren: './pages/attendance/attendance.module#AttendancePageModule' },
  { path: 'events', loadChildren: './events/events.module#EventsPageModule' },
 //{ path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  
  { path: 'photos', loadChildren: './pages/photos/photos.module#PhotosPageModule' },
  
  
  
 
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
