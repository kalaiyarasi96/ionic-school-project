import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/map';
import { Response } from '@angular/http'


@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  otp:any;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    let ReceivedData = window.localStorage.getItem('vijay'); 
    var html = " ";
    html += ReceivedData;

    /* alert(ReceivedData); */
    $(".mobilenum").html(html);
  }
  
  gotohomepage() 
  {
    
   /*  let body = new FormData();
    let headerOptions: any = { 'Content-Type': 'application/json' };
    let headers = new Headers(headerOptions);
    let options = new RequestOptions({ headers: headers }); */
    let ReceivedData = window.localStorage.getItem('vijay'); 
   
    var body = new FormData();
    body.append('otp', this.otp);
    body.append('mobile', ReceivedData );

   

    var apiUrl = environment.apiUrl+'parentapi/verifyotp';

    this.http.post(apiUrl,body,{
      headers: {'Content-Type':'application/json'}
      })
      this.http.post(apiUrl, body)

          .subscribe(data => {
          console.log(data);

          if(data["status"] == "success") 

          {
            console.log(data["token"])
            localStorage.setItem('token', data["token"]);
           
            window.location.href = "http://localhost:8100/home";

          }
        });  
       // console.log(data["status"]);

      
        
  }
}
