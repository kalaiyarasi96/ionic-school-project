import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-flashnews',
  templateUrl: './flashnews.page.html',
  styleUrls: ['./flashnews.page.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FlashnewsPage implements OnInit {
  data: any;

 
  constructor(private http: HttpClient,public navCtrl: NavController) { }

  student() 
  {
//token pass header details
var token = localStorage.getItem("token");
console.log(token);
var apiUrl = environment.apiUrl+'parentapi/getchildren';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
  
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
    
     html +='<ion-select-option>'+val.name+'</ion-select-option>';
    
    
   
 
            
     });
    
     $(".studentname").html(html);
     /* alert("studentname"); */

   }

 });

 
  }
  
  ngOnInit() {


//token pass header details
var token = localStorage.getItem("token");
console.log(token);
var apiUrl = environment.apiUrl+'parentapi/flashnews';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
     html +='<div class="holder"><ul id="ticker01"><li><a href="#">'+val.flash_news+'</a></li></ul></div>';
            
     });
     $(".jsonvalue").html(html);


   }

 });

 


   /*  this.http.get('http://localhost:8080/sms2016_backup/index.php?parents/flashapi/')
    .subscribe(data => {this.data = data; console.log(this.data); 
      var html = " ";
      this.data.data.forEach(function(val) {
      var keys = Object.keys(val);
  
    html +='<div class="holder"><ul id="ticker01"><li><span>'+val.uploaded_on+'</span> <a href="#">'+val.flash_news+'</a></li></ul></div>';
             
      });
      $(".jsonvalue").html(html);
    
    });
   */
 
 
  }
  
 }