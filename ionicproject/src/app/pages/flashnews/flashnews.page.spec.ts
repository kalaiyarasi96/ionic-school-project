import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlashnewsPage } from './flashnews.page';

describe('FlashnewsPage', () => {
  let component: FlashnewsPage;
  let fixture: ComponentFixture<FlashnewsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlashnewsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlashnewsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
