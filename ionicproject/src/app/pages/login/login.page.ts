import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  mobile:any;
  constructor(private http: HttpClient) {}

  ngOnInit() {}
  sendOTP() 
  {
    var body = new FormData();
    body.append('mobile', this.mobile);
    
     /*let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers }); */

    var apiUrl = environment.apiUrl+'parentapi/sendotp';

    this.http.post(apiUrl,body,{
      headers: {'Content-Type':'application/json'}
      })
      this.http.post(apiUrl, body)

       /*  this.http.post(apiUrl, body, options) */
       // .map(res => res)
        .subscribe(data => {
          console.log(data);
          //alert(data["status"]);
          if(data["status"] == "success") {
            window.localStorage.setItem('vijay',this.mobile); 
            window.location.href = "http://localhost:8100/otp";

          }
       
        });
        
  } 
 
}
