import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';






@Component({
  selector: 'app-homework',
  templateUrl: './homework.page.html',
  styleUrls: ['./homework.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeworkPage implements OnInit {

  select_id:any;
  data: any;
  semSelected:any;
 constructor(private http: HttpClient){ }

  student() 
  {

//token pass header details
var token = localStorage.getItem("token");
/* console.log(token); */
var apiUrl = environment.apiUrl+'parentapi/getchildren';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   /* alert('123'); */
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
  
     
     html +='<ion-select-option *ngFor="count" value='+val.student_id+','+val.name+'>'+val.name+'</ion-select-option>';
    
     /*  alert(val.student_id) */
     
     $(".studentname").html(html);
     //$(".studentname").attr('student_id', val.student_id);
    
     /*  alert(val.student_id);  */
     
    
   

     });

    
   }

 });
 
  }

  getPostEntry(event){

    var selectBothIds = this.semSelected;  
   /*  alert(selectBothIds); */
   
   let ids = selectBothIds.split(","); 
   let student_id = ids[0];
   let stu_name = ids[1]; 
    

    var body = new FormData();
    body.append('student_id',student_id);
 
    var token = localStorage.getItem("token");
    console.log(token);

    var apiUrl = environment.apiUrl+'parentapi/homework';
 
    
   this.http.post(apiUrl,body,{
   headers: {'Authorization':token}
   })

   
   .map(res => res)
   .subscribe(data => {
     this.data = data;
     console.log(data);
          if (data["status"] == "success") {
          
           var html = " ";
           this.data.data.forEach(function(val1) {
           var keys = Object.keys(val1);
           const date = new Date(val1.sms_date_time);  // 2009-11-10
           const month = date.toLocaleString('default', { month: 'short' });
           console.log(month);
          /*  alert(month); */
           html +='<ion-card><div id="ionic-drp"><ion-grid><ion-row><ion-col size="4" class="ion-float-left"><div class="stu-name"><p>'+stu_name+'</p></div><div class="special-t"><p>MH</p></div></ion-col><ion-col size="8" class="ion-float-right"> <div style="float:right;"><ion-button  class="special-t1  text-cetner"><ul><li>'+month+'</li><li>19</li></ul></ion-button></div></ion-col><div class="span-text"><span class=" span-t ion-float-right"  class="toggle">open</span></div> </ion-row> </ion-grid> </div></ion-card><div class="myContent"><ion-card>'+val1.sms_message+'</ion-card></div>';
              
         
           });
           $(".jsonvalue").html(html);
    
    
         }  
       });
       $(".flip").click(function(){
        $(".panel").slideToggle("slow");
      });
  }
  
    
 
 ngOnInit() {}

 
}

