import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';



@Component({
  selector: 'app-videgallery',
  templateUrl: './videgallery.page.html',
  styleUrls: ['./videgallery.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VidegalleryPage implements OnInit {
  

  /*video: any [] = [
  {
    title: 'Awesome video',
    url: 'https://www.youtube.com/embed/9B7te184ZpQ?rel=0',
    
},
{
  title: ' video show',
  url: 'https://www.youtube.com/embed/MLleDRkSuvk',
  
},
]*/
  select_id:any;
  data: any;
  semSelected:any;
 constructor(private http: HttpClient){ }

  student() 
  {

//token pass header details
var token = localStorage.getItem("token");
/* console.log(token); */
var apiUrl = environment.apiUrl+'parentapi/getchildren';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   /* alert('123'); */
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
  
     
     html +='<ion-select-option *ngFor="count" value='+val.class_id+','+val.student_id+'>'+val.name+'</ion-select-option>';
    
     /*  alert(val.student_id) */
     
     $(".studentname").html(html);
     //$(".studentname").attr('student_id', val.student_id);
    
     /*  alert(val.student_id);  */
     
    
   

     });

    
   }

 });
 
  }

  getPostEntry(event){

    let selectBothIds = this.semSelected; 
    let ids = selectBothIds.split(","); 
    let class_id = ids[0]; 
    let student_id = ids[1];

    /* alert(class_id);
    alert(student_id); */

    //alert(dataaa["student_id"]);
    let body = new FormData();
    body.append('class_id', class_id);
    body.append('student_id', student_id);
        
    var token = localStorage.getItem("token");
    console.log(token);

    var apiUrl = environment.apiUrl+'parentapi/videoshow';
 

   this.http.post(apiUrl,body,{
   headers: {'Authorization':token}
   })

     
   .map(res => res)
   .subscribe(data => {
     this.data = data;
     console.log(data);
         if (data["status"] == "success") {

           var html = " ";
           this.data.data.forEach(function(val1) {
           var keys = Object.keys(val1);
           html +='<ion-grid><ion-row><ion-col col-12><iframe id="video" class="video-size" src='+val1.url+' frameborder="0" allowfullscreen></iframe></ion-col></ion-row></ion-grid>';
              
          /*  alert(val1.url); */
           });
           $(".jsonvalue").html(html);
    
    
         }
       });

  }

 ngOnInit() {}


}
