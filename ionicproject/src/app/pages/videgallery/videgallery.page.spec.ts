import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VidegalleryPage } from './videgallery.page';

describe('VidegalleryPage', () => {
  let component: VidegalleryPage;
  let fixture: ComponentFixture<VidegalleryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VidegalleryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VidegalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
