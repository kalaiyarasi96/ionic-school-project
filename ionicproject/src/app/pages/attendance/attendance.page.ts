import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
 styleUrls: ['./attendance.page.scss'],
 encapsulation: ViewEncapsulation.None
})


export class AttendancePage implements OnInit {
  header_data:any;
  data: any
  constructor(private http: HttpClient,public navCtrl: NavController) {
  
   }

  ngOnInit() {

//token pass header details
     var token = localStorage.getItem("token");
     console.log(token);
     var apiUrl = environment.apiUrl+'parentapi/teacherlist';
     this.http.get(apiUrl,{
     headers: {'Content-Type':'application/json','Authorization':token}
    })
      .map(res => res)
      .subscribe(data => {
        this.data = data;
        console.log(data);
        if (data["status"] == "success") {

          var html = " ";
          this.data.data.forEach(function(val) {
          var keys = Object.keys(val);
         /*  html +='<ion-item style="border-bottom: 1px solid  #F3F3F3 !important"><ion-avatar slot="start"><img src="http://localhost:8080/sms2016_backup/uploads/teacher_image/'+val.teacher_img+'"></ion-avatar><ion-label><h2>'+val.name+'</h2><p> '+val.email+'</p></ion-label></ion-item>'; */
                 
          });
          $(".jsonvalue").html(html);


        }

      });

  }

}

