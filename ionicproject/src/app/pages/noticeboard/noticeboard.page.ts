import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';




@Component({
  selector: 'app-noticeboard',
  templateUrl: './noticeboard.page.html',
  styleUrls: ['./noticeboard.page.scss'],
  encapsulation: ViewEncapsulation.None
})


export class NoticeboardPage implements OnInit {
  data: any;
  constructor(private http: HttpClient){ }

  ngOnInit() {
   
    

//token pass header details
var token = localStorage.getItem("token");
console.log(token);
var apiUrl = environment.apiUrl+'parentapi/noticeboard';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
   html +='<ion-card><div class="ions-content"><div class="col-md-12"><ul> <li><a><ion-icon name="clipboard" class="ion-i"></ion-icon></a></li><li class="col-l"><a>Holiday</a></li><li style="float:right"class="col-r"><a>date</a></li></ul><p>'+val.notice_title+'</p></div></div></ion-card>';
            
     });
     $(".jsonvalue").html(html);


   }

 });



   
   
}


}