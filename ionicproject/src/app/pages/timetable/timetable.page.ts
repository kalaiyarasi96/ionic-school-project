import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.page.html',
  styleUrls: ['./timetable.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TimetablePage implements OnInit {
  
  select_id:any;
  data: any;
  semSelected:any;
  monday:any;
 constructor(private http: HttpClient){ }

  student() 
  {

//token pass header details
var token = localStorage.getItem("token");
/* console.log(token); */
var apiUrl = environment.apiUrl+'parentapi/getchildren';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   /* alert('123'); */
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
     html +='<ion-select-option *ngFor="count" value='+val.student_id+'>'+val.name+'</ion-select-option>';
         
     $(".studentname").html(html);
    
     });

    
   }

 });
 
  }


   
  getPostEntry(event){

    let ids1 = this.semSelected; 
    let student_id = ids1[0];

   
    let body = new FormData();
   
    body.append('student_id', student_id);
        
    var token = localStorage.getItem("token");
    console.log(token);

    var apiUrl = environment.apiUrl+'parentapi/classroutine ';
 

   this.http.post(apiUrl,body,{
   headers: {'Authorization':token}
   })

     
   .map(res => res)
   .subscribe(data => {
     this.data = data;
     console.log(data);
         
       });

  }
   
  mon(){


    
  }
 
 ngOnInit() {}

 
}

