import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
  encapsulation: ViewEncapsulation.None
})


export class GalleryPage implements OnInit {

  data: any
  constructor(private http: HttpClient) { }

  ngOnInit() {

//token pass header details
     var token = localStorage.getItem("token");
     console.log(token);
     var apiUrl = environment.apiUrl+'parentapi/photoshow';
     this.http.get(apiUrl,{
     headers: {'Content-Type':'application/json','Authorization':token}
    })
      .map(res => res)
      .subscribe(data => {
        this.data = data;
        console.log(data);
        if (data["status"] == "success") {

          var html = " ";
          this.data.data.forEach(function(val) {
          var keys = Object.keys(val);
          html +='<ion-row><ion-col><a button href="/photos"><ion-card><ion-item><ion-label class="ion-text-center">'+val.event_name+'</ion-label></ion-item></ion-card></a></ion-col></ion-row>';
                 
          });
          $(".jsonvalue").html(html);


        }

      });

  }

}