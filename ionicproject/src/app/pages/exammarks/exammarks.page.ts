import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-exammarks',
  templateUrl: './exammarks.page.html',
  styleUrls: ['./exammarks.page.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ExammarksPage implements OnInit {

  select_id:any;
  data: any;
  semSelected:any;
  examselected:any;
 constructor(private http: HttpClient){ }

  student() 
  {

//token pass header details
var token = localStorage.getItem("token");
/* console.log(token); */
var apiUrl = environment.apiUrl+'parentapi/getchildren';
this.http.get(apiUrl,{
headers: {'Content-Type':'application/json','Authorization':token}
})
 .map(res => res)
 .subscribe(data => {
   this.data = data;
   /* alert('123'); */
   console.log(data);
   if (data["status"] == "success") {

     var html = " ";
     this.data.data.forEach(function(val) {
     var keys = Object.keys(val);
  
     
     html +='<ion-select-option *ngFor="count" value='+val.student_id+'>'+val.name+'</ion-select-option>';
    
     /*  alert(val.student_id) */
     
     $(".studentname").html(html);
    
     //$(".studentname").attr('student_id', val.student_id);
    
     /*  alert(val.student_id);  */
     
    
   

     });

    
   }

 });
 
  }

  getPostEntry(event){

    var selectBothIds = this.semSelected;  
   /* alert(selectBothIds);   */
   
   /* let ids = selectBothIds.split(",");  */
   let student_id = selectBothIds[0];
   /* alert(student_id); */
  
    var body = new FormData();
    body.append('student_id',student_id);
   
 
    var token = localStorage.getItem("token");
    console.log(token);

    var apiUrl = environment.apiUrl+'parentapi/exams';
     
    
   this.http.post(apiUrl,body,{
   headers: {'Authorization':token}
   })

   
   .map(res => res)
   .subscribe(data => {
     this.data = data;
     console.log(data);
          if (data["status"] == "success") {
          
           var html = "";
     
           this.data.data.forEach(function(val1) {
           var keys = Object.keys(val1);
         
  
          html +='<ion-select-option *ngFor="count" value='+val1.exam_id+','+student_id+'>'+val1.name+'</ion-select-option> ';
          
        /*   alert(student_id); */
          
           $(".jsonvalue").html(html);
          /*  alert(val1.name);  */
          }); 
     
          
       // $(".jsonvalue").attr('exam_id', val1.exam_id);  */
     /*  alert(val.student_id) */
     
    
    
     /*  alert(val.student_id);  */
    
         } 
        
       });
      
  }
  
 

  showmark(event){
 

  let selectBothIds1 = this.examselected; 
  /* alert(selectBothIds1); */
  let ids1 = selectBothIds1.split(","); 
  let exam_id = ids1[0]; 
 /*  alert(exam_id); */
  let student_id = ids1[1];
  /* alert(student_id); */
  let body = new FormData();
  body.append('exam_id', exam_id);
  body.append('student_id', student_id);
  
    var token = localStorage.getItem("token");
    console.log(token);
 
    
    var apiUrl = environment.apiUrl+'parentapi/marks';
     
    
   this.http.post(apiUrl,body,{
   headers: {'Authorization':token}
   })

   
   .map(res => res)
   .subscribe(data => {
     this.data = data;
     console.log(data);
          if (data["status"] == "success") {
          
            var html = " ";

           this.data.data.forEach(function(val1) {
           var keys = Object.keys(val1);
         
           if (data["status"] === "") {

           html +='<ion-card><ion-item><ion-icon name="contacts" slot="start"></ion-icon><ion-label>Class</ion-label><span class="ion-float-right">'+val1.class_name+'</span> </ion-item></ion-card><ion-card><ion-item><ion-icon name="bookmarks" slot="start"></ion-icon> <ion-label>Subject</ion-label><span class="ion-float-right">'+val1.subject_name+'</span> </ion-item></ion-card><ion-card><ion-item><ion-icon name="stats" slot="start"></ion-icon><ion-label>Total_mark</ion-label><span class="ion-float-right">'+val1.mark_total+'</span> </ion-item></ion-card><ion-card><ion-item>  <ion-icon name="save" slot="start"></ion-icon><ion-label>Mark Obtained</ion-label><span class="ion-float-right">'+val1.mark_obtained+'</span> </ion-item></ion-card><ion-card><ion-item> <ion-icon name="clipboard" slot="start"></ion-icon><ion-label>Comment</ion-label><span class="ion-float-right">'+val1.comment+'</span> </ion-item></ion-card>';
        
           }           
           else {
             
            html +='<ion-card><ion-item><ion-icon name="contacts" slot="start"></ion-icon><ion-label>Class</ion-label><span class="ion-float-right"></span></ion-item></ion-card>';
           }
         
           });
          
           $(".tableselect").html(html);
    
    
         }  
       });
      
  }

  

 ngOnInit() {

  
    

  

 }
 

 
}

