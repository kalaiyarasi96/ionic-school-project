import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Http, Headers, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.page.html',
  styleUrls: ['./teacher.page.scss'],
  encapsulation: ViewEncapsulation.None
})



export class TeacherPage implements OnInit {
  data: any
  constructor(private http: HttpClient) { }

  ngOnInit() {

//token pass header details
     var token = localStorage.getItem("token");
     console.log(token);
     var apiUrl = environment.apiUrl+'parentapi/teacherlist';
     this.http.get(apiUrl,{
     headers: {'Content-Type':'application/json','Authorization':token}
    })
      .map(res => res)
      .subscribe(data => {
        this.data = data;
        console.log(data);
        if (data["status"] == "success") {

          var html = " ";
          this.data.data.forEach(function(val) {
          var keys = Object.keys(val);
          html +='<ion-item><ion-avatar slot="start"><img src="'+environment.apiUrl+'uploads/teacher_image/'+val.teacher_img+'"></ion-avatar><ion-label><h2>'+val.name+'</h2><p> '+val.email+'</p></ion-label></ion-item>';
               
          
               });
          $(".jsonvalue").html(html);


        }

      });

  }

}

